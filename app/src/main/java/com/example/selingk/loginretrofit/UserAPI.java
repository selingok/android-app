package com.example.selingk.loginretrofit;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Selin on 22.08.2016.
 */
public interface UserAPI {

    @POST("/delete")
    public void deleteAccount(@Body UserModel user, Callback<ResponseModel> callback);

    @POST("/update/{newPass}")
    public void updatePassword(@Body UserModel user, @Path("newPass") String newPass, Callback<ResponseModel> callback);

}
